
//  Add a method to enable function cloning
Function.prototype.clone = function() {
    var cloneObj = this;
    if(this.__isClone) {
      cloneObj = this.__clonedFrom;
    }

    var temp = function() { return cloneObj.apply(this, arguments); };
    for(var key in this) {
        temp[key] = this[key];
    }

    temp.__isClone = true;
    temp.__clonedFrom = cloneObj;

    return temp;
};

//  Enable other syntax for this widget
var $jaqlService = prism.$injector.get('base.services.jaqlService');
var originalParser = $jaqlService.compileFormula.clone();
$jaqlService.compileFormula = function(ds, formula, context){
    if (prism.$ngscope.widget.type == "filterbuttons"){
        //  Stub in a fake formula
        formula = "1";    
    } 
    return originalParser(ds,formula,context)
}

prism.registerWidget("filterbuttons", {

    name: "filterbuttons",
    family: "filter",
    title: "Filter Buttons",
    iconSmall: "/plugins/filterbuttons/icon-24.png",
    styleEditorTemplate: "/plugins/filterbuttons/styler.html",
    hideNoResults: true,
    style: {
        align: 'center',
        filterSetting: 'Clear All Filters',
        textSize: 16,
        width: 200
    },
    directive: {
        desktop: "filterbuttons"
    },
    data: {
        selection: [],
        defaultQueryResult: {},
        panels: [
            {
                name : config.panel.name,
                type: 'visible',    
                itemAttributes: ["color"],
                metadata : {
                    types : ['measures'],
                    maxitems : -1
                },
                allowedColoringTypes: function() {
					return  {
						color: true,
						condition: false,
						range: false
					};
				},
                itemAdded: function(widget, item) {
                    //  Add in a default color
                    item.format = {
                        'color': {
                            'color': config.default.color,
                            'type': 'color'
                        }
                    }
                }
            }, 
            {
                name: 'filters',
                type: 'filters',
                metadata: {
                    types: ['dimensions'],
                    maxitems: -1
                }
            }
        ],

        canColor: function (widget, panel, item) {			

			//	Handle the break by
			return true;
        },

        allocatePanel : function (widget, metadataItem) {
            if (prism.$jaql.isMeasure(metadataItem) ) {
                return config.panel.name;
            }
        },

        // ranks the compatibility of the given metadata items with the widget
        rankMetadata : function (items, type, subtype) {
            return -1;
        },

        // populates the metadata items to the widget
        populateMetadata : function (widget, items) {
            var a = prism.$jaql.analyze(items);
            widget.metadata.panel(config.panel.name).push(a.measures);
            widget.metadata.panel("filters").push(a.filters);
        }
    },
    options: {
        dashboardFiltersMode: "slice",
        selector: false,
        title: false
    },
    sizing: {

        minHeight: 128, //header
        maxHeight: 2048,
        minWidth: 128,
        maxWidth: 2048,
        height: 64,
        defaultWidth: 512
    }
});

