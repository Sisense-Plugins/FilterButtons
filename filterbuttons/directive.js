
//  Define the controller for the modal window
mod.controller('filterbuttonsController', ['$scope',  function ($scope) {

    //  Grab the scope
    var myScope = $scope;      

    //  List of buttons (initially empty array)
    myScope.buttons = [];
    myScope.lastButtonPressed = null;

    //  Function to convert a hex value to RGB
    function hexToRgb(hex){

        var r = parseInt(hex.slice(1, 3), 16),
            g = parseInt(hex.slice(3, 5), 16),
            b = parseInt(hex.slice(5, 7), 16);

        return {
            'red': r,
            'green': g,
            'blue': b
        }
    }

    //  Function that figures out an appropriate text color, based on the background color
    function calculateTextColor(backgroundColor){

        //  Define the output options
        var white = 'white',
            black = 'black';

        //  Get the red, green, and blue values of this color
        var color = hexToRgb(backgroundColor);

        // Calculate the brightness of the color
        var o = Math.round(((color.red * 299) + (color.green * 587) + (color.blue* 114)) /1000); 

        // If the background is too dark, color the font white
        if ( o <=125 ){
            return white;
        } else {
            return black;
        }
    }

    //  Function to build the list of buttons
    myScope.buttonBuilder = function(items){

        //  List of buttons (initially empty array)
        myScope.buttons = [];
        myScope.lastButtonPressed = null;

        //  Get the widget object
        var widget =  myScope.$parent.widget;
        
        //  Loop through each metadata item, and create a simplified button object
        items.forEach(function(item){

            //  Make sure the syntax is valid
            var isValid = item.jaql && item.jaql.type == 'measure' && item.jaql.context;
            if (isValid){

                //  Create a button object
                var button = {
                    label: item.jaql.title,
                    style: widget.style,
                    filters: []
                }

                //  Get the background and text colors
                var color = $$get(item,'format.color.color',config.default.color);
                button.backgroundColor = color;
                button.textColor = calculateTextColor(color);
                button.isSelected = false;

                //  Loop through the jaql's context, to look for filters
                for (key in item.jaql.context){

                    //  Make sure this item exists
                    if (item.jaql.context.hasOwnProperty(key)){
                        
                        //  Get the filter item
                        var filterItem = item.jaql.context[key];

                        //  Only add this if its a filter
                        if (filterItem.filter){
                            button.filters.push(filterItem);
                        }
                    }
                }

                //  Save the button to my scope
                myScope.buttons.push(button);
            }
        })    
    }

    //  Function that determines if a bookmark can be deleted
    myScope.clicked = function(button,widget){

        //  Check to see if the same button was pressed twice
        var doubleClicked = (myScope.lastButtonPressed && (button.label == myScope.lastButtonPressed.label));

        //  Save a reference to this button, so it can be cleared
        myScope.lastButtonPressed = doubleClicked ? null : button;
        
        //  Get the dashboard's filters object
        var dashboard = prism.$ngscope.dashboard;

        if (!doubleClicked && widget.style.filterSetting == 'Clear All Filters'){
            //  Clear all Filters before setting selections
            dashboard.filters.clear()
        } else if (!doubleClicked && widget.style.filterSetting == 'Reset to Defaults'){
            //  Reset to default filters, before setting selections
            dashboard.filters.reset();
        }

        //  Define the filter settings to use
        var settings = {
            'save':true,
            'refresh':false,
            'unionIfSameDimensionAndSameType':true
        };

        //  Loop through each filter
        button.filters.forEach(function(jaql){
            
            //  Create the filter object
            var myFilter = {
                'jaql': $.extend({},jaql)
            };

            //  Reset the filter, if doubleclicked
            if (doubleClicked){
                myFilter.jaql.filter = {
                    'all': true,
                    'explicit': false,
                    'multiSelection': true
                }
            }

            //  Apply to the dashboard
            dashboard.filters.update(myFilter,settings)
        })

        //  Refresh the dashboard
        dashboard.refresh();

        button.isSelected = !doubleClicked;
    }

    //  Set a watch on the metadata items, so we redraw the buttons on every change
    $scope.existingButtons = $scope.widget.metadata.panel(0).items;
    $scope.$watchCollection('existingButtons', function (buttons) {        
        myScope.buttonBuilder(buttons)
    });
}])

mod.directive('filterbuttons', [
    function ($timeout, $dom) {
        return {
            priority: 0,
            replace: false,
            templateUrl: "/plugins/filterbuttons/template.html",
            transclude: false,
            restrict: 'E',
            link: function ($scope, lmnt, attrs) {  }
        }
    }
])

mod.controller('filterbuttonsStylerController', ['$scope',
    
    function ($scope) {

        /**
         * watches
         */

        $scope.$watch('widget', function (val) {
            $scope.model = $$get($scope, 'widget.style');
        });

        /**
         * public methods
         */    

        // Function to just redraw the widget
        $scope.refreshWidget = function(){
            //redraw widget
            _.defer(function () {
                $scope.$root.widget.redraw();
            });
        }

        $scope.changeFilterSetting = function (setting) {

            //apply changes
            $scope.filterSetting = setting;

            //save changes to widget
            $$set($scope.$root.widget, 'style.filterSetting', $scope.filterSetting);

            //redraw widget
            _.defer(function () {
                $scope.$root.widget.redraw();
            });
        };

        $scope.changeAlign = function (align) {

            //apply changes
            $scope.align = align;

            //save changes to widget
            $$set($scope.$root.widget, 'style.align', $scope.align);

            //redraw widget
            _.defer(function () {
                $scope.$root.widget.redraw();
            });
        };


    }
]);